exports.HTML_TEMPLATE = ({
    title,
    genres,
    totalPages,
    author,
    authorAddress,
}) => {
    return `
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="utf-8">
          <title>NodeMailer Email Template</title>
          <style>
            .container {
              width: 100%;
              height: 100%;
              padding: 20px;
              background-color: #f4f4f4;
            }
            .email {
              width: 80%;
              margin: 0 auto;
              background-color: #fff;
              padding: 20px;
            }
            .email-header {
              background-color: #333;
              color: #fff;
              padding: 20px;
              text-align: center;
            }
            .email-body {
              padding: 20px;
            }
            .email-footer {
              background-color: #333;
              color: #fff;
              padding: 20px;
              text-align: center;
            }
          </style>
        </head>
        <body>
          <div class="container">
            <div class="email">
              <div class="email-header">
                <h1>${title}</h1>
              </div>
              <div class="email-body">
                <table>
                  <tr>
                    <th>Genres:</th>
                    <td>${genres}</td>
                  </tr>
                  <tr>
                    <th>Total Pages:</th>
                    <td>${totalPages}</td>
                  </tr>
                  <tr>
                    <th>Author:</th>
                    <td>${author}</td>
                  </tr>
                  <tr>
                    <th>Author Address:</th>
                    <td>${authorAddress}</td>
                  </tr>
                </table>
              </div>
              <div class="email-footer">
                <p>Thank You!</p>
              </div>
            </div>
          </div>
        </body>
      </html>
    `;
};
