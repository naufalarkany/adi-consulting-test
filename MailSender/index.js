require('dotenv').config();
const express = require('express');
const nodemailer = require('nodemailer');
const mail = require('./mailTemplate');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const { PORT, SMTP_USER, SMTP_PASSWORD, SMTP_SERVICE, SMTP_HOST, SMTP_PORT } =
    process.env;

const transporter = nodemailer.createTransport({
    service: SMTP_SERVICE,
    host: SMTP_HOST,
    port: SMTP_PORT,
    secure: false,
    auth: {
        user: SMTP_USER,
        pass: SMTP_PASSWORD,
    },
});

app.get('/', (req, res) => {
    return res.status(200).send({
        status: 'success',
        msg: 'Mail Sender Service running well',
    });
});

app.post('/send-mail', async (req, res) => {
    try {
        const {
            to,
            subject,
            title,
            genres,
            totalPages,
            author,
            authorAddress,
        } = req.body;
        const info = await transporter.sendMail({
            from: `Mail Service <${SMTP_USER}>`, // sender address
            to,
            subject,
            html: mail.HTML_TEMPLATE({
                title,
                genres,
                totalPages,
                author,
                authorAddress,
            }),
        });

        return res.status(200).send({
            status: 'success',
            msg: 'Email sent successfully',
            messageId: info.messageId,
        });
    } catch (error) {
        return res.status(500).send({
            status: 'failed',
            msg: error.message,
        });
    }
});

const start = async () => {
    try {
        app.listen(PORT, () =>
            console.log(`Server is listening on port ${PORT}...`)
        );
    } catch (error) {
        console.log(error);
    }
};

start();
