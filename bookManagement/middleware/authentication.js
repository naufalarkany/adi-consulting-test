const jwt = require('jsonwebtoken');
const { UnauthorizedError } = require('../utils/errors');
const { User } = require('../models');

const authenticationMiddleware = async (req, res, next) => {
    try {
        const authHeader = req.headers.authorization;
        if (!authHeader || !authHeader.startsWith('Bearer ')) {
            throw new UnauthorizedError('No token provided');
        }
        const token = authHeader.split(' ')[1];
        const decoded = jwt.verify(token, process.env.JWT_TOKEN_SECRET);
        console.log(decoded);
        const { id, tokenVersion } = decoded;
        const user = await User.findByPk(id);
        if (!user || user.tokenVersion !== tokenVersion)
            throw new UnauthorizedError('No authorized');
        req.user = { id, tokenVersion };
        return next();
    } catch (error) {
        next(error);
    }
};

module.exports = authenticationMiddleware;
