const errorHandler = (err, req, res, next) => {
    return res.status(err.statusCode || 500).send({
        status: 'failed',
        msg: err.message || 'Internal Server Error',
        errors: err.data,
    });
};

module.exports = errorHandler;
