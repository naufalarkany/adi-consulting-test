const invalidPathHandler = (req, res, next) => {
    return res.status(404).send({
        status: 'failed',
        msg: 'Please check again your API',
    });
};

module.exports = invalidPathHandler;
