const { Op } = require('sequelize');
const { Book, Genre } = require('../models');
const BadRequestError = require('../utils/errors/badRequestError');
const { validateRequest, genreScheme } = require('../utils/validation');

exports.create = async (req, res, next) => {
    try {
        const payload = validateRequest(req.body, genreScheme);
        const genre = await Genre.create(payload);
        return res.status(201).send({
            status: 'success',
            msg: 'Genre Successfully Created',
            data: genre,
        });
    } catch (error) {
        next(error);
    }
};

exports.getAll = async (req, res, next) => {
    try {
        const { q, sortBy, order } = req.query;
        const genres = await Genre.findAll({
            where: {
                ...(q && {
                    name: {
                        [Op.iLike]: `%${q}%`,
                    },
                }),
            },
            order: [[sortBy || 'name', order || 'ASC']],
        });
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully Get Genres',
            data: genres,
        });
    } catch (error) {
        next(error);
    }
};

exports.getById = async (req, res, next) => {
    try {
        const { genreId } = req.params;
        const genre = await Genre.findByPk(genreId);
        if (!genre) throw new BadRequestError('Genre Not Found');
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully Get Genre',
            data: genre,
        });
    } catch (error) {
        next(error);
    }
};

exports.update = async (req, res, next) => {
    try {
        const { genreId } = req.params;
        const payload = validateRequest(req.body, genreScheme);
        const genre = await Genre.findByPk(genreId);
        if (!genre) throw new BadRequestError('Genre Not Found');
        await genre.update(payload);
        await genre.reload();
        return res.status(200).send({
            status: 'success',
            msg: 'Genre Successfully Updated',
            data: genre,
        });
    } catch (error) {
        next(error);
    }
};

exports.destroy = async (req, res, next) => {
    try {
        const { genreId } = req.params;
        const genre = await Genre.findByPk(genreId, {
            include: {
                model: Book,
                through: {
                    attributes: [],
                },
            },
        });
        if (!genre) throw new BadRequestError('Genre Not Found');
        if (genre.Books.length > 0)
            throw new BadRequestError('Genre have published book');
        await genre.destroy();
        return res.status(200).send({
            status: 'success',
            msg: 'Genre Successfully Deleted',
        });
    } catch (error) {
        next(error);
    }
};
