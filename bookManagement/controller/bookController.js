const { Op } = require('sequelize');
const axios = require('axios');
const { Book, Author, Genre, BookGenre, sequelize } = require('../models');
const BadRequestError = require('../utils/errors/badRequestError');
const { validateRequest, bookScheme } = require('../utils/validation');

exports.create = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
        const { title, authorId, totalPages, genreIds } = validateRequest(
            req.body,
            bookScheme
        );
        const author = await Author.findByPk(authorId, { transaction: t });
        if (!author) {
            throw new BadRequestError('Author not found');
        }
        const book = await Book.create(
            {
                title,
                authorId,
                totalPages,
            },
            { transaction: t }
        );
        for (const genreId of genreIds) {
            const genre = await Genre.findByPk(genreId, { transaction: t });
            if (!genre) {
                throw new BadRequestError('Genre not found');
            }
            await BookGenre.create(
                {
                    bookId: book.id,
                    genreId: genre.id,
                },
                { transaction: t }
            );
        }
        await t.commit();
        return res.status(201).send({
            status: 'success',
            msg: 'Book Successfully Created',
            data: book,
        });
    } catch (error) {
        await t.rollback();
        next(error);
    }
};

exports.getAll = async (req, res, next) => {
    try {
        const { q, sortBy, order, authorId, genreId } = req.query;
        const books = await Book.findAll({
            where: {
                ...(q && {
                    title: {
                        [Op.iLike]: `%${q}%`,
                    },
                }),
            },
            include: [
                {
                    model: Author,
                    where: {
                        ...(authorId && {
                            id: Number(authorId),
                        }),
                    },
                },
                {
                    model: Genre,
                    through: {
                        attributes: [],
                    },
                    where: {
                        ...(genreId && {
                            id: Number(genreId),
                        }),
                    },
                },
            ],
            order: [[sortBy || 'title', order || 'ASC']],
        });
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully Get Books',
            data: books,
        });
    } catch (error) {
        next(error);
    }
};

exports.getById = async (req, res, next) => {
    try {
        const { bookId } = req.params;
        const book = await Book.findByPk(bookId, {
            include: [
                {
                    model: Author,
                },
                {
                    model: Genre,
                    through: {
                        attributes: [],
                    },
                },
            ],
        });
        if (!book) {
            throw new BadRequestError('Book not found');
        }
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully Get Books',
            data: book,
        });
    } catch (error) {
        next(error);
    }
};

exports.update = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
        const { bookId } = req.params;
        const { title, authorId, totalPages, genreIds } = validateRequest(
            req.body,
            bookScheme
        );
        const book = await Book.findByPk(bookId, {
            include: [
                {
                    model: Author,
                },
                {
                    model: Genre,
                    through: {
                        attributes: [],
                    },
                },
            ],
            transaction: t,
        });

        if (!book) {
            throw new BadRequestError('Book not found');
        }
        const _book = book.toJSON();
        if (authorId) {
            const author = await Author.findByPk(authorId, { transaction: t });
            if (!author) {
                throw new BadRequestError('Author not found');
            }
        }
        if (genreIds) {
            for (const currentGenre of _book.Genres) {
                const bookGenre = await BookGenre.findOne({
                    where: {
                        [Op.and]: [
                            { bookId: _book.id },
                            { genreId: currentGenre.id },
                        ],
                    },
                    transaction: t,
                });
                await bookGenre.destroy({ transaction: t });
            }
            for (const genreId of genreIds) {
                const genre = await Genre.findByPk(genreId, { transaction: t });
                if (!genre) {
                    throw new BadRequestError('Genre not found');
                }
                await BookGenre.create(
                    {
                        bookId: _book.id,
                        genreId: genre.id,
                    },
                    { transaction: t }
                );
            }
        }
        await book.update(
            {
                title,
                authorId,
                totalPages,
            },
            { transaction: t }
        );
        await t.commit();
        await book.reload({
            include: [
                {
                    model: Author,
                },
                {
                    model: Genre,
                    through: {
                        attributes: [],
                    },
                },
            ],
            order: [['createdAt', 'DESC']],
        });
        return res.status(200).send({
            status: 'success',
            msg: 'Book Successfully Updated',
            data: book,
        });
    } catch (error) {
        await t.rollback();
        next(error);
    }
};

exports.destroy = async (req, res, next) => {
    const t = await sequelize.transaction();
    try {
        const { bookId } = req.params;
        const book = await Book.findByPk(bookId, {
            include: [
                {
                    model: Genre,
                    through: {
                        attributes: [],
                    },
                },
            ],
            transaction: t,
        });
        if (!book) {
            throw new BadRequestError('Book not found');
        }
        const _book = book.toJSON();
        if (_book.Genres) {
            for (const currentGenre of _book.Genres) {
                const bookGenre = await BookGenre.findOne({
                    where: {
                        [Op.and]: [
                            { bookId: _book.id },
                            { genreId: currentGenre.id },
                        ],
                    },
                    transaction: t,
                });
                await bookGenre.destroy({ transaction: t });
            }
        }
        await book.destroy({ transaction: t });
        await t.commit();
        return res.status(200).send({
            status: 'success',
            msg: 'Book Successfully Deleted',
        });
    } catch (error) {
        await t.rollback();
        next(error);
    }
};

exports.sendDetail = async (req, res, next) => {
    try {
        const { bookId } = req.params;
        const book = await Book.findByPk(bookId, {
            include: [
                {
                    model: Author,
                },
                {
                    model: Genre,
                    through: {
                        attributes: [],
                    },
                },
            ],
        });
        if (!book) {
            throw new BadRequestError('Book not found');
        }
        const _book = book.toJSON();
        const genres = _book.Genres.map((e) => e.name).join(', ');
        const data = {
            to: req.body.to,
            subject: 'Detail Buku - ' + _book.title,
            title: _book.title,
            genres,
            totalPages: _book.totalPages,
            author: _book.Author.name,
            authorAddress: _book.Author.address,
        };
        const response = await axios.post(
            process.env.MAIL_SENDER_BASE_URL,
            data
        );
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully Get Books',
            data,
            messageId: response.data.messageId,
        });
    } catch (error) {
        next(error);
    }
};
