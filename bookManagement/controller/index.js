const book = require('./bookController');
const author = require('./authorController');
const genre = require('./genreController');
const auth = require('./authController');

module.exports = {
    book,
    author,
    genre,
    auth,
};
