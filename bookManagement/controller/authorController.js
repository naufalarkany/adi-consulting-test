const { Op } = require('sequelize');
const { Book, Author } = require('../models');
const BadRequestError = require('../utils/errors/badRequestError');
const { validateRequest, authorScheme } = require('../utils/validation');
exports.create = async (req, res, next) => {
    try {
        const data = validateRequest(req.body, authorScheme);
        const author = await Author.create(data);
        return res.status(201).send({
            status: 'success',
            msg: 'Author Successfully Created',
            data: author,
        });
    } catch (error) {
        next(error);
    }
};

exports.getAll = async (req, res, next) => {
    try {
        const { q, sortBy, order } = req.query;
        const authors = await Author.findAll({
            where: {
                ...(q && {
                    [Op.or]: [
                        {
                            name: {
                                [Op.iLike]: `%${q}%`,
                            },
                        },
                        {
                            address: {
                                [Op.iLike]: `%${q}%`,
                            },
                        },
                    ],
                }),
            },
            order: [[sortBy || 'name', order || 'ASC']],
        });
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully Get Authors',
            data: authors,
        });
    } catch (error) {
        next(error);
    }
};

exports.getById = async (req, res, next) => {
    try {
        const { authorId } = req.params;
        const author = await Author.findByPk(authorId);
        if (!author) throw new BadRequestError('Author Not Found');
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully Get Author',
            data: author,
        });
    } catch (error) {
        next(error);
    }
};

exports.update = async (req, res, next) => {
    try {
        const { authorId } = req.params;
        const data = validateRequest(req.body, authorScheme);
        const author = await Author.findByPk(authorId);
        if (!author) throw new BadRequestError('Author Not Found');
        await author.update(data);
        await author.reload();
        return res.status(200).send({
            status: 'success',
            msg: 'Author Successfully Updated',
            data: author,
        });
    } catch (error) {
        next(error);
    }
};

exports.destroy = async (req, res, next) => {
    try {
        const { authorId } = req.params;
        const author = await Author.findByPk(authorId, {
            include: {
                model: Book,
            },
        });
        if (!author) throw new BadRequestError('Author Not Found');
        if (author.Books.length > 0)
            throw new BadRequestError('Author have published book');
        await author.destroy();
        return res.status(200).send({
            status: 'success',
            msg: 'Author Successfully Deleted',
        });
    } catch (error) {
        next(error);
    }
};
