const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models');

const { UnauthorizedError, BadRequestError } = require('../utils/errors');
const {
    validateRequest,
    registerScheme,
    loginScheme,
} = require('../utils/validation');

exports.register = async (req, res, next) => {
    try {
        const payload = validateRequest(req.body, registerScheme);
        const user = await User.findOne({
            where: {
                email: payload.email,
            },
        });
        if (user) throw new BadRequestError('Email already registered');
        const createdUser = await User.create(payload);
        const _createdUser = createdUser.toJSON();
        delete _createdUser.password;
        return res.status(201).send({
            status: 'success',
            msg: 'User Successfully created',
            data: _createdUser,
        });
    } catch (error) {
        next(error);
    }
};

exports.login = async (req, res, next) => {
    try {
        const { email, password } = validateRequest(req.body, loginScheme);
        const user = await User.findOne({
            where: {
                email,
            },
        });
        if (!user || !bcrypt.compareSync(password, user.password)) {
            throw new UnauthorizedError(
                'Please check again your username and password'
            );
        }
        const token = jwt.sign(
            {
                id: user.id,
                tokenVersion: user.tokenVersion,
            },
            process.env.JWT_TOKEN_SECRET,
            {
                expiresIn: process.env.JWT_TOKEN_EXPIRES_IN,
            }
        );
        return res.status(200).send({
            status: 'success',
            msg: 'Successfully login',
            data: {
                token,
            },
        });
    } catch (error) {
        next(error);
    }
};

exports.revoke = async (req, res, next) => {
    try {
        const userId = req.user.id;
        const user = await User.findByPk(userId);
        await user.update({ tokenVersion: user.tokenVersion + 1 });
        return res.status(200).send({
            status: 'success',
            msg: 'Authentication Successfully Revoked',
        });
    } catch (error) {
        next(error);
    }
};
