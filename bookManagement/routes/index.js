const { Router } = require('express');
const authenticationMiddleware = require('../middleware/authentication');
const authRouter = require('./authRoutes');
const bookRouter = require('./bookRoutes');
const authorRouter = require('./authorRoutes');
const genreRouter = require('./genreRoutes');
const router = Router();

router.use('/auth', authRouter);
router.use('/book', authenticationMiddleware, bookRouter);
router.use('/author', authenticationMiddleware, authorRouter);
router.use('/genre', authenticationMiddleware, genreRouter);

module.exports = router;
