const { Router } = require('express');
const { auth } = require('../controller');
const authenticationMiddleware = require('../middleware/authentication');
const router = Router();

router.post('/register', auth.register);
router.post('/login', auth.login);
router.post('/revoke', authenticationMiddleware, auth.revoke);
module.exports = router;
