const { Router } = require('express');
const { author } = require('../controller');
const router = Router();

router.post('/', author.create);
router.get('/', author.getAll);
router.get('/:authorId', author.getById);
router.put('/:authorId', author.update);
router.delete('/:authorId', author.destroy);
module.exports = router;
