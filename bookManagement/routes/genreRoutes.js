const { Router } = require('express');
const { genre } = require('../controller');
const router = Router();

router.post('/', genre.create);
router.get('/', genre.getAll);
router.get('/:genreId', genre.getById);
router.put('/:genreId', genre.update);
router.delete('/:genreId', genre.destroy);
module.exports = router;
