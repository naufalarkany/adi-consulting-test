const { Router } = require('express');
const { book } = require('../controller');
const router = Router();

router.post('/', book.create);
router.get('/', book.getAll);
router.get('/:bookId', book.getById);
router.put('/:bookId', book.update);
router.delete('/:bookId', book.destroy);
router.post('/:bookId/send', book.sendDetail);
module.exports = router;
