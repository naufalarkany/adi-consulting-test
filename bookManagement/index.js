require('dotenv').config();
const express = require('express');
const router = require('./routes');
const errorHandler = require('./middleware/errorHandler');
const invalidPathHandler = require('./middleware/invalidPathHandler');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

require('./utils/runLogger')(app);

//use for main router
app.use('/api', router);

//just for test API
app.get('/heatlhcheck', (req, res, next) => {
    try {
        return res.status(200).send({
            status: 'success',
            msg: 'API book-management running well',
        });
    } catch (error) {
        next(error);
    }
});
app.use(errorHandler);
app.use(invalidPathHandler);

// App starting here
const port = process.env.PORT || 3001;
const start = async () => {
    try {
        app.listen(port, () =>
            console.log(`Server is listening on port ${port}...`)
        );
    } catch (error) {
        console.log(error);
    }
};

start();
