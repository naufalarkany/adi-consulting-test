const { Model } = require('sequelize');
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    User.init(
        {
            email: DataTypes.STRING,
            password: {
                type: DataTypes.STRING,
                set(value) {
                    this.setDataValue('password', bcrypt.hashSync(value, 10));
                },
            },
            name: DataTypes.STRING,
            tokenVersion: DataTypes.INTEGER,
        },
        {
            sequelize,
            modelName: 'User',
        }
    );
    return User;
};
