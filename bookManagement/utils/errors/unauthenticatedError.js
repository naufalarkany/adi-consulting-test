const CustomError = require('./customError');
class UnauthenticatedError extends CustomError {
    constructor(message) {
        super(message);
        this.statusCode = 403;
    }
}

module.exports = UnauthenticatedError;
