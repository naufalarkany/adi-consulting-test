const Joi = require('joi');

const loginScheme = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(8).required(),
});

const registerScheme = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(8).required(),
    confirmPassword: Joi.ref('password'),
    name: Joi.string().min(8).required(),
});

module.exports = {
    loginScheme,
    registerScheme,
};
