const Joi = require('joi');

const authorScheme = Joi.object({
    name: Joi.string().min(3).required(),
    address: Joi.string().min(3).required(),
});

module.exports = {
    authorScheme,
};
