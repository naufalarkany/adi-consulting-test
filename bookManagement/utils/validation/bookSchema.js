const Joi = require('joi');

const bookScheme = Joi.object({
    title: Joi.string().min(3).required(),
    authorId: Joi.number().required(),
    totalPages: Joi.number().required(),
    genreIds: Joi.array().items(Joi.number()).required(),
});

module.exports = {
    bookScheme,
};
