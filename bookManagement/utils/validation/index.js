const { BadRequestError } = require('../errors');
const { loginScheme, registerScheme } = require('./authSchema');
const { authorScheme } = require('./authorSchema');
const { bookScheme } = require('./bookSchema');
const { genreScheme } = require('./genreSchema');

const validateRequest = (reqBody, schema) => {
    const { error, value } = schema.validate(reqBody, { abortEarly: false });

    if (error) {
        const errorMessages = error.details.map((err) => err.message);
        throw new BadRequestError('Validation Error', errorMessages);
    } else {
        return value;
    }
};
module.exports = {
    validateRequest,
    loginScheme,
    registerScheme,
    authorScheme,
    bookScheme,
    genreScheme,
};
