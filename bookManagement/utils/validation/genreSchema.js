const Joi = require('joi');

const genreScheme = Joi.object({
    name: Joi.string().min(3).required(),
});

module.exports = {
    genreScheme,
};
