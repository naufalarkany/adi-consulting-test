# Book Management

Untuk dokumentasi API klik [disini](https://documenter.getpostman.com/view/14914411/2s9Y5cuLgZ) or <https://documenter.getpostman.com/view/14914411/2s9Y5cuLgZ>

Untuk jalankan API

# Service Sending Email

Buatlah file .env dan isi value sesuai environment kalian masing masing.

1. Buka folder MailSender
2. npm install
3. npm start

# Service Management

Buatlah file .env dan isi value sesuai environment kalian masing masing.

1. buat folder kosong dengan nama "logger"
2. npm install
3. npm run db:create
4. npm run db:migrate
5. npm start

Terima kasih!
